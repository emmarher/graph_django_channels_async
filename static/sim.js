const ctx = document.getElementById('myChart').getContext('2d');
//const baseLine = new Array(50).fill('.');
console.log(baseLine);
var graphData = {
    type: 'line', //'bar'
    data: {
      display:true,
        labels: baseLine,
        datasets: [{
            label: 'Volumen (mL)',
            data: baseLine,
						fill: true,
            backgroundColor: [
                'rgb(75, 192, 192)',
            ],
						borderColor: 'rgb(75, 192, 192)',
						tension: 0.2,
            //borderWidth: 1.5
        }]
    },
    //data: baseLine,
    options: {
			scales: {
            y: {
                beginAtZero: false,
                stacked: true
            },
            
        }
			}
}
const myChart = new Chart(ctx,graphData );


do{
// Aqui el onchange

let v = simulaTor();

}
while(true);











function simulaTor(C) {
    //let C = 0.1;
    let R = 3; //"Resistencia de la vía aerea"
    let T = R*C;
    let fs = 10; // es la tasa de muestreo
    let Ts = 1/fs;
    let K = C; //"Compliancia"
    let alfa = Math.pow(2.718281,-Ts/T);
    let t = 1.17; //tiempo de muestreo 
    let n = t/Ts;
    let N = parseInt(n);
    let aux = 10;
    let con = 1;
    let p = new Array(parseInt(N)).fill('0');
    let v = new Array(parseInt(N)).fill('0');
    let q = new Array(parseInt(N)).fill('0');
    let s = new Array(parseInt(N)).fill('0');
    let kTs = new Array(parseInt(N)).fill('0');
    let e = new Array(parseInt(N)).fill('0');

    console.log(v);
    for (let k=0; k<s.length; k++) {
                p[k] = aux;
                kTs[k] = k*Ts;
                e[k] = 1;
                if (k==0){
                    s[k]=0;
                    v[k] = 0;}
                else{
                    s[k] = alfa*s[k-1] + K*(1-alfa)*e[k-1];}
                if (con >= 0.5/T){
                    con = 1;
                    if (p[k] == 10){
                        aux = 0;}
                    else
                        {aux = 10;}
                }
                else{ 
                    con = con + 1;}
                kTs[k] = k*Ts; //para acceder al elemento del array k(n)
            
                if (k==1){
                    v[k] = 0;}
                else{
                    v[k] = alfa*v[k-1] + K*(1-alfa)*p[k-1];
                    q[k] = (v[k] - v[k-1])/Ts; //flujo - derivada
                }
            }
    v[0]=0; // Se agrega esto ya que al primer valor le agrega un NaN

    return v;
    console.log('Valores del array V',v);
    console.log('Valores del array kTs',kTs);
    console.log('Valores del array e',e);
}