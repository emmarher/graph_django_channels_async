from django.urls import path
from .views import index, chart, simulator, simulador, simulador_live, half_breath, buffer_hf, half_breath_v2

urlpatterns = [
	path('',index),
	path('chart',chart),
	path('simulator',simulator), #BufferConsumer
	path('simulador', simulador),
	path('live', simulador_live),
	path('simuladorv2', half_breath),
 	path('svm', buffer_hf),
	path('simuladorv3', half_breath_v2),
 
]