import json

import math
from random import randint
import numpy as np
#from time import sleep # Cambiamos este sleep por un asyncio
from asyncio import sleep
#importar el websocket consumer asincrono
from channels.generic.websocket import AsyncWebsocketConsumer

#No asincrono
#from channels.generic.websocket import WebsocketConsumer
"""
	Para llamar cuaquier funcion desde el nucleo del ruteo tenemos que usar
	await, vease el ejemplo de uso abajo
"""
class GraphConsumer(AsyncWebsocketConsumer):


	new_value = 1
	on_connected = False
	group_name=0;
	
	async def connect(self):
		self.username =  "Anonymous"
		self.group_name=str(randint(0,1000))
		#print('-- kwargs',self.scope['url_route']['kwargs'])
		#print('channel name: ',self.channel_name)
		await self.channel_layer.group_add(self.group_name, self.channel_name)
		print('nombre del grupo: ', self.group_name)
		#define que hace cuando se conecta el channel, aceptamos la conexion del navegador
		await self.accept()

		print('Inicio el envio de datos')
		token_socket = self.scope['headers'][10][1]
		print('TOKEN SOCKET: ' , token_socket)

		self.on_connected = True
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': 0.06,
				'resistence':8,
				'lastV': 0,
			}
		)
		

	async def receive(self, text_data):
		# To receive data from client
		data = json.loads(text_data)
		print('Data received:', data)
		new_c = float(data['message'])/1000
		new_r = int(data['resistence'])
		last_v=data['lastV']
		#print('last Value: ',last_v)
		self.new_value = float(data['message'])
		#print('Se sobreescribio el valor nuevo desde el frontend',self.new_value)
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': new_c,
				'resistence': new_r,
				'lastV': last_v,
			}
		)

	async def disconnect(self, close_code):
		print('<x> Disconnecting...', close_code)
		await self.channel_layer.group_discard(self.group_name, self.channel_name)
		self.on_connected = False
		self.close()

	async def deprocessing(self, event):
		# Receive message from client and send to group
		val_other = event['message']
		await self.send(json.dumps({'value': val_other}))


	async def simulate_volume(self,event):
		#C = 0.1
		C = float(event['message'])
		#print('event: %s' % event['message'])
		#print((event))
		l_v = event['lastV']
		#print('last_v = %f' % l_v)
		#R = 3#"Resistencia de la vía aerea"
		R = int(event['resistence'])
		T = R*C
		fs = 40 # es la tasa de muestreo
		Ts = 1/fs
		T=T
		K = C#"Compliancia"

		alfa = np.exp(-Ts/T)
		t = 1 #tiempo de muestreo 
		n = t/Ts #fs
		N = int(n)
		aux = 10
		p = 1
		con = 1
		p = [0 for i in range(int(N))]
		v = [l_v for i in range(int(N))]
		q = [0 for i in range(int(N))]
		s = [0 for i in range(int(N))]
		kTs = [0 for i in range(int(N))]
		e = [0 for i in range(int(N))]

		for k in range(N):
			p[k] = aux
			kTs[k] = k*Ts
			e[k] = 1
			if k==-10:
				s[k]=0
			else:
				s[k] = alfa*s[k-1] + K*(1-alfa)*e[k-1]
			if con >= 0.5/Ts:
				con = 1
				if p[k] == 10:
					aux = 0
				else:
					aux = 10
			else: 
				con = con + 1
			kTs[k] = k*Ts #para acceder al elemento del array k(n)
		
			if k==-10:
				v[k] = 0
			else:
				v[k] = alfa*v[k-1] + K*(1-alfa)*p[k-1]
				q[k] = (v[k] - v[k-1])/Ts #flujo - derivada
			
			await self.send(json.dumps({'volume':v[k], 'presion':p[k], 'flujo':q[k]}))
			await sleep(Ts) #0.22
		#await self.send(json.dumps({'volume':v, 'presion':p, 'flujo':q}))

class BufferConsumer(AsyncWebsocketConsumer):

	new_value = 1
	group_name=0;
	
	async def connect(self):
		self.group_name=str(randint(0,1000))
		#print('-- kwargs',self.scope['url_route']['kwargs'])
		#print('channel name: ',self.channel_name)
		await self.channel_layer.group_add(self.group_name, self.channel_name)
		print('nombre del grupo: ', self.group_name)
		#define que hace cuando se conecta el channel, aceptamos la conexion del navegador
		await self.accept()
		# Or accept the connection and specify a chosen subprotocol.
        # A list of subprotocols specified by the connecting client
        # will be available in self.scope['subprotocols']

		print('Inicio el envio de datos')
		token_socket = self.scope['headers'][10][1]
		print('TOKEN SOCKET: ' , token_socket)

		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': 0.06,
				'resistence':8,
				'lastV': 0,
			}
		)
		

	async def receive(self, text_data):
		# To receive data from client
		data = json.loads(text_data)
		#print('Data received:', data)
		new_c = float(data['message'])/1000
		new_r = int(data['resistence'])
		last_v=data['lastV']
		#print('last Value: ',last_v)
		#self.new_value = float(data['message'])
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': new_c,
				'resistence': new_r,
				'lastV': last_v,
			}
		)

	async def disconnect(self, close_code):
		print('<x> Disconnecting...', close_code)
		await self.channel_layer.group_discard(self.group_name, self.channel_name)
		self.close()

	async def simulate_volume(self,event):
		#C = 0.1
		C = float(event['message'])
		#print('event: %s' % event['message'])
		#print((event))
		l_v = event['lastV']
		#print('last_v = %f' % l_v)
		#R = 3#"Resistencia de la vía aerea"
		R = int(event['resistence'])
		T = R*C
		fs = 20 # es la tasa de muestreo
		Ts = 1/fs
		T=T
		K = C#"Compliancia"

		alfa = np.exp(-Ts/T)
		t = 1 #tiempo de muestreo 
		n = t/Ts
		N = int(n)
		aux = 10
		p = 1
		con = 1
		p = [0 for i in range(int(N))]
		v = [l_v for i in range(int(N))]
		q = [0 for i in range(int(N))]
		s = [0 for i in range(int(N))]
		kTs = [0 for i in range(int(N))]
		e = [0 for i in range(int(N))]

		for k in range(N):
			p[k] = aux
			kTs[k] = k*Ts
			e[k] = 1
			if k==-10:
				s[k]=0
			else:
				s[k] = alfa*s[k-1] + K*(1-alfa)*e[k-1]
			if con >= 0.5/Ts:
				con = 1
				if p[k] == 10:
					aux = 0
				else:
					aux = 10
			else: 
				con = con + 1
			kTs[k] = k*Ts #para acceder al elemento del array k(n)
		
			if k==-10:
				v[k] = 0
			else:
				v[k] = alfa*v[k-1] + K*(1-alfa)*p[k-1]
				q[k] = (v[k] - v[k-1])/Ts #flujo - derivada
			
			#await self.send(json.dumps({'volume':v[k], 'presion':p[k], 'flujo':q[k]}))
			#await sleep(0.14) #0.22
		await self.send(json.dumps({'volume':v, 'presion':p, 'flujo':q}))

class Half_Breath(AsyncWebsocketConsumer):


	new_value = 1
	on_connected = False
	group_name=0;
	variable_global = 0
	
	async def connect(self):
		self.username =  "Anonymous"
		self.group_name=str(randint(0,1000))
		#print('-- kwargs',self.scope['url_route']['kwargs'])
		#print('channel name: ',self.channel_name)
		await self.channel_layer.group_add(self.group_name, self.channel_name)
		#print('nombre del grupo: ', self.group_name)
		#define que hace cuando se conecta el channel, aceptamos la conexion del navegador
		await self.accept()

		print('Inicio el envio de datos')
		token_socket = self.scope['headers'][10][1]
		print('TOKEN SOCKET: ' , token_socket)

		self.on_connected = True
		print('Valor de new_value: ' , self.new_value)
		print('valor de variable_global', self.variable_global)
  
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': 0.06,
				'resistence':8,
				'lastV': 0,
				'con': 1,
				'presion': 0,
				's': 0,
				'kts': 0,
				'p_up': True,
				'rpm': 60
			}
		)
		

	async def receive(self, text_data):
		# To receive data from client
		data = json.loads(text_data)
		#print('Data received:', data)
		new_c = float(data['message'])/1000
		new_r = int(data['resistence'])
		last_v=data['lastV']
		con = data['con']
		presion = data['presion']
		kTs = data['kts']
		s = data['s']
		p_up = data['p_up']
		rpm = data['rpm']
		print('p_up Value: ',p_up)
		self.new_value = float(data['message'])
		self.variable_global = new_r
		print('Se sobreescribio el valor nuevo de compliancia desde el frontend ',self.new_value)
		print('Se sobreescribio el valor nuevo de resistencia desde el frontend ',self.variable_global)
    
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': new_c,
				'resistence': new_r,
				'lastV': last_v,
				'con': con,
				'presion': presion,
				'kts': kTs,
				's': s,
				'p_up': p_up,
				'rpm':rpm
			}
		)

	async def disconnect(self, close_code):
		print('<x> Disconnecting...', close_code)
		await self.channel_layer.group_discard(self.group_name, self.channel_name)
		self.on_connected = False
		self.close()


	async def simulate_volume(self,event):
		#C = 0.1
		C = float(event['message'])
		#print('event: %s' % event['message'])
		#print((event))
		rpm = int(event['rpm'])
		p_up = event['p_up']
		l_kts = event['kts']
		l_s = event['s']
		l_v = event['lastV']
		l_p = event['presion'] #last previous presion value
		#print('last_v = %f' % l_v)
		#R = 3#"Resistencia de la vía aerea"
		R = int(event['resistence'])
		con = int(event['con'])
		T = R*C
		fs = 20 # es la tasa de muestreo
		Ts = 1/fs
		K = C#"Compliancia"
		
		fr= (0.5*(60/rpm))/Ts #Frecuencia respiratoria

		alfa = np.exp(-Ts/T)
		t = 0.5 #tiempo de muestreo 
		n = t/Ts
		N = int(n)
		aux = 10
		#con = 1
		p = [l_p for i in range(int(N))] #Presion
		v = [l_v for i in range(int(N))] # Volume
		q = [0 for i in range(int(N))] # Flujo
		s = [l_s for i in range(int(N))]
		kTs = [l_kts for i in range(int(N))]
		e = [0 for i in range(int(N))]

		for k in range(N):
			if p_up:
				aux = 10
			else:
				aux = 0

			kTs[k] = k*Ts
			e[k] = 1
			if k==0:
				s[k]=0
			else:
				s[k] = alfa*s[k-1] + K*(1-alfa)*e[k-1]
			if con >= fr: # 0.5/Ts
				con = 1
				p_up  = not p_up
			#	if p[k] == 10:
			#		aux = 0
			#	else:
			#		aux = 10
			else: 
				con = con + 1
			kTs[k] = k*Ts #para acceder al elemento del array k(n)
			p[k] = aux
		
			if k==-1: # ==1
				v[k] = 0
			else:
				v[k] = alfa*v[k-1] + K*(1-alfa)*p[k-1]
				q[k] = (v[k] - v[k-1])/Ts #flujo - derivada
			
			await self.send(json.dumps({'volume':v[k], 'presion':p[k], 'flujo':q[k], 'con':con, 's':s[k], 'kts':kTs[k], 'p_up':p_up}))
			await sleep(Ts) #0.22
   
class Half_Breath_v2(AsyncWebsocketConsumer):


	new_value = 1
	on_connected = False
	group_name=0;
	variable_global = 0
	p_up_global = True
	
	async def connect(self):
		self.username =  "Anonymous"
		self.group_name=str(randint(0,1000))
		#print('-- kwargs',self.scope['url_route']['kwargs'])
		#print('channel name: ',self.channel_name)
		await self.channel_layer.group_add(self.group_name, self.channel_name)
		#print('nombre del grupo: ', self.group_name)
		#define que hace cuando se conecta el channel, aceptamos la conexion del navegador
		await self.accept()

		print('Inicio el envio de datos')
		token_socket = self.scope['headers'][10][1]
		print('TOKEN SOCKET: ' , token_socket)

		self.on_connected = True
		print('Valor de new_value: ' , self.new_value)
		print('valor de variable_global', self.variable_global)
  
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_presion',
				'message': 0.06,
				'resistence':8,
				'lastV': 0,
				'con': 1,
				'presion': 0,
				's': 0,
				'kts': 0,
				'p_up': True,
				'rpm': 60
			}
		)
		

	async def receive(self, text_data):
		# To receive data from client
		data = json.loads(text_data)
		#print('Data received:', data)
		new_c = float(data['message'])/1000
		new_r = int(data['resistence'])
		last_v=data['lastV']
		con = data['con']
		presion = data['presion']
		kTs = data['kts']
		s = data['s']
		p_up = data['p_up']
		rpm = data['rpm']
		
		self.new_value = float(data['message'])
		self.variable_global = new_r
		print('Se sobreescribio el valor nuevo de compliancia desde el frontend ',self.new_value)
		print('Se sobreescribio el valor nuevo de resistencia desde el frontend ',self.variable_global)
    
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_presion',
				'message': new_c,
				'resistence': new_r,
				'lastV': last_v,
				'con': con,
				'presion': presion,
				'kts': kTs,
				's': s,
				'rpm':rpm,
				'p_up': p_up,
			}
		)

	async def disconnect(self, close_code):
		print('<x> Disconnecting...', close_code)
		await self.channel_layer.group_discard(self.group_name, self.channel_name)
		self.on_connected = False
		self.close()

	async def simulate_presion(self,event):

		C = float(event['message'])
		rpm = int(event['rpm'])
		l_kts = event['kts']
		l_s = event['s']
		l_v = event['lastV']
		l_p = event['presion'] #last previous presion value
		R = int(event['resistence'])
		con = int(event['con'])
		T = R*C
		fs = 20 # es la tasa de muestreo
		Ts = 1/fs
		K = C#"Compliancia"
		fr= (0.5*(60/rpm))/Ts #Frecuencia respiratoria
		print('fr: ',fr)
		p_up = event['p_up']
		alfa = np.exp(-Ts/T)
		t = 0.5 #tiempo de muestreo 
		n = t/Ts
		N = int(n)
		aux = 10
		p = [l_p for i in range(int(N))] #Presion
		print(p_up)
		print(con)
		s = [l_s for i in range(int(N))]
		kTs = [l_kts for i in range(int(N))]
		e = [0 for i in range(int(N))]

		for k in range(N):
			if p_up:
				aux = 10
			else:
				aux = 0

			kTs[k] = k*Ts
			e[k] = 1
			if k==0:
				s[k]=0
			else:
				s[k] = alfa*s[k-1] + K*(1-alfa)*e[k-1]
			
			if con >= fr: # 0.5/Ts
				con = 1
				p_up  = not p_up

			else:
				con = con + 1
			
			kTs[k] = k*Ts #para acceder al elemento del array k(n)
			p[k] = aux
		
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': C,
				'resistence': R,
				'lastV': l_v,
				'con': con,
				'presion': p,
				'kts': kTs,
				's': s,
				'rpm':rpm,
				'p_up': p_up,
			}
		)
		


	async def simulate_volume(self,event):
		#C = 0.1
		C = float(event['message'])
		#print('event: %s' % event['message'])
		#print((event))
		rpm = int(event['rpm'])
		#p_up = event['p_up']
		l_kts = event['kts']
		l_s = event['s']
		l_v = event['lastV']
		p = event['presion'] #last previous presion value
		print('Presion en volumen: ',p)
		#print('last_v = %f' % l_v)
		#R = 3#"Resistencia de la vía aerea"
		R = int(event['resistence'])
		con = int(event['con'])
		T = R*C
		fs = 20 # es la tasa de muestreo
		Ts = 1/fs
		K = C#"Compliancia"
		
		fr= (0.5*(60/rpm))/Ts #Frecuencia respiratoria
		p_up = event['p_up']
		alfa = np.exp(-Ts/T)
		t = 0.5 #tiempo de muestreo 
		n = t/Ts
		N = int(n)
		aux = 10
		#con = 1
		#p = [l_p for i in range(int(N))] #Presion
		v = [l_v for i in range(int(N))] # Volume
		q = [0 for i in range(int(N))] # Flujo
		s = [l_s for i in range(int(N))]
		kTs = [l_kts for i in range(int(N))]
		e = [0 for i in range(int(N))]

		for k in range(N):
			if p_up:
				aux = 10
			else:
				aux = 0

			kTs[k] = k*Ts
			e[k] = 1
			if k==0:
				s[k]=0
			else:
				s[k] = alfa*s[k-1] + K*(1-alfa)*e[k-1]
			if con >= fr: # 0.5/Ts
				con = 1
				p_up = not p_up
			#	if p[k] == 10:
			#		aux = 0
			#	else:
			#		aux = 10
			else: 
				con = con + 1
			kTs[k] = k*Ts #para acceder al elemento del array k(n)
			#p[k] = aux
		
			if k==-1: # ==1
				v[k] = 0
			else:
				v[k] = alfa*v[k-1] + K*(1-alfa)*p[k-1]
				q[k] = (v[k] - v[k-1])/Ts #flujo - derivada
			
			await self.send(json.dumps({'volume':v[k], 'presion':p[k], 'flujo':q[k], 'con':con, 's':s[k], 'kts':kTs[k], 'p_up':p_up}))
			await sleep(Ts) #0.22
   

class Buffer_Half_Breath(AsyncWebsocketConsumer):


	new_value = 1
	on_connected = False
	group_name=0;
	variable_global = 0
	p_up_g = True
	

	
	flujo = lambda self, current_v, past_v, Ts : (current_v - past_v)/Ts
     
	# Derivada de flujo
	
 
	async def connect(self):
		self.username =  "Anonymous"
		self.group_name=str(randint(0,1000))
		#print('-- kwargs',self.scope['url_route']['kwargs'])
		#print('channel name: ',self.channel_name)
		await self.channel_layer.group_add(self.group_name, self.channel_name)
		print('nombre del grupo: ', self.group_name)
		#define que hace cuando se conecta el channel, aceptamos la conexion del navegador
		await self.accept()

		print('Inicio el envio de datos')
		token_socket = self.scope['headers'][10][1]
		print('TOKEN SOCKET: ' , token_socket)

		self.on_connected = True
  
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_presion',
				'message': 0.06,
				'resistence':8,
				'lastV': 0,
				'con': 1,
				'presion': 0,
				#'p_up': True,
				'rpm': 60
			}
		)
		

	async def receive(self, text_data):
		# To receive data from client
		data = json.loads(text_data)
		#print('Data received:', data)
		new_c = float(data['message'])/1000
		new_r = int(data['resistence'])
		last_v=data['lastV']
		con = data['con']
		presion = data['presion']
		#kTs = data['kts']
		#s = data['s']
		#p_up = data['p_up']
		rpm = data['rpm']
		#print('p up Value: ',p_up)
		self.new_value = float(data['message'])
		self.variable_global = new_r
		#print('Se sobreescribio el valor nuevo de compliancia desde el frontend ',self.new_value)
		#print('Se sobreescribio el valor nuevo de resistencia desde el frontend ',self.variable_global)
    
		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_presion',
				'message': new_c,
				'resistence': new_r,
				'lastV': last_v,
				'con': con,
				'presion': presion,
				#'p_up': p_up,
				'rpm':rpm
			}
		)

	async def disconnect(self, close_code):
		print('<x> Disconnecting...', close_code)
		await self.channel_layer.group_discard(self.group_name, self.channel_name)
		self.on_connected = False
		self.close()
  
	async def simulate_presion(self,event):
		C = float(event['message'])
		#print('event: %s' % event['message'])
		#print((event))
		rpm = int(event['rpm'])
		#p_up = event['p_up']
		l_v = event['lastV']
	
		l_p = int(event['presion']) #last previous presion value
		R = int(event['resistence'])
		con = int(event['con'])		
		T = R*C
		fs = 20 # es la tasa de muestreo
		Ts = 1/fs
		
		fr= (0.5*(60/rpm))/Ts #Frecuencia respiratoria
		t = 0.5 #tiempo de muestreo 
		n = t/Ts
		N = int(n)
		
		p = [l_p for i in range(int(N))] #Presion
		#v = [l_v for i in range(int(N))] # Volume
		#q = [0 for i in range(int(N))] # Flujo
		e = [0 for i in range(int(N))]
		#s = [0 for i in range(int(N))]
		#kTs = [0 for i in range(int(N))]
		for k in range(N):
			if self.p_up_g:
				aux = 10
			else:
				aux = 0			
			e[k] = 1
			if con >= fr: # 0.5/Ts
				con = 1
				self.p_up_g  = not self.p_up_g
			else: 
				con = con + 1
			p[k] = aux

		await self.channel_layer.group_send(
			self.group_name,
			{
				'type': 'simulate_volume',
				'message': C,
				'resistence': R,
				'lastV': l_v,
				'con': con,
				'presion': p,
				#'p_up': p_up,
				'rpm':rpm
			}
		)
		
	async def simulate_volume(self,event):
		#C = 0.1
		C = float(event['message'])
		#print('event: %s' % event['message'])
		#print((event))
		rpm = int(event['rpm'])
		#p_up = event['p_up']
		l_v = event['lastV']
	
		p = event['presion'] #last previous presion value
		R = int(event['resistence'])
		con = int(event['con'])
		T = R*C
		fs = 20 # es la tasa de muestreo
		Ts = 1/fs
		K = C#"Compliancia"
		fr= (0.5*(60/rpm))/Ts #Frecuencia respiratoria
		alfa = np.exp(-Ts/T)
		t = 0.5 #tiempo de muestreo 
		n = t/Ts
		N = int(n)
		
		#p = [l_p for i in range(int(N))] #Presion
		v = [l_v for i in range(int(N))] # Volume
		q = [0 for i in range(int(N))] # Flujo
		e = [0 for i in range(int(N))]
		#s = [0 for i in range(int(N))]
		#kTs = [0 for i in range(int(N))]


		for k in range(N):
			#if p_up:
			#	aux = 10
			#else:
			#	aux = 0

			#kTs[k] = k*Ts
			e[k] = 1

			#if con >= fr: # 0.5/Ts
			#	con = 1
			#	p_up  = not p_up
			#	if p[k] == 10:
			#		aux = 0
			#	else:
			#		aux = 10
			#else: 
			#	con = con + 1
			#kTs[k] = k*Ts #para acceder al elemento del array k(n)
			#p[k] = aux
		
			if k==0: # ==1
				v[k] = alfa*v[k-1] + K*(1-alfa)*p[k]
				#q[k] = self.flujo(v[k], v[-1], Ts)
				q[k] = (v[k] - v[k-1])/Ts #flujo - derivada
			else:
				v[k] = alfa*v[k-1] + K*(1-alfa)*p[k-1]
				#q[k] = self.flujo(v[k], v[-1], Ts)
				q[k] = (v[k] - v[k-1])/Ts #flujo - derivada
			
			#await self.send(json.dumps({'volume':v[k], 'presion':p[k], 'flujo':q[k], 'con':con, 's':s[k], 'kts':kTs[k], 'p_up':p_up}))
			#await sleep(Ts) #0.22
		await self.send(json.dumps({'volume':v, 'presion':p, 'flujo':q, 'con':con}))
   