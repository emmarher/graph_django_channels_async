from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, 'base.html', context={'text':'Started Simulator Ehecatl 4T'})

def chart(request):
	return render(request, 'chart.html', context={'text':'Hi CHarts'})

def simulator(request):
	return render(request, 'simulador.html', context={'text':'Simulador de ventilador V1.2 Buffer'})

def simulador(request):
	return render(request, 'simuladorv1.html', context={'text':'Simulador de ventilador V1'})

def simulador_live(request):
	return render(request, 'simuladorTime.html', context={'text':'Simulador de ventilador V1.3 Live'})

def half_breath(request):
	return render(request, 'halfBreath.html', context={'text':'Simulador de ventilador V2.0 Simulate Half Breath'})

def buffer_hf(request):
    return render(request, 'bufferHf.html', context={'text':'Simulador de Ventilador Mecánico v3.0'})

def half_breath_v2(request):
    return render(request, 'halfBreath_v2.html', context={'text':'Simulador de Ventilador Mecánico  Half-Breath v2'})