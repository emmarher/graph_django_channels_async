#Ruteo/Path del websocket

from django.urls import path
from .consumers import GraphConsumer, BufferConsumer, Half_Breath, Buffer_Half_Breath, Half_Breath_v2


#handlers son llmados views pero en channels son consumers

ws_urlpatterns = [
	path('ws/graph/', GraphConsumer.as_asgi()),
	path('ws/buffer/', BufferConsumer.as_asgi()), #Otra version del socket, enviando por buffer
	path('ws/halfbreath/', Half_Breath.as_asgi()),
 	path('ws/buffer_hf/', Buffer_Half_Breath.as_asgi()), #
  	path('ws/halfbreath_v2/', Half_Breath_v2.as_asgi()),
]