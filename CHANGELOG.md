## v[2.1.0](author: emmarher)
  - Add new version, combined half_breath version to modifiy alone.
  - Change and Add function, Add simulate presion function, after from there call simulate volume(and 'flujo'), after send json to frontend.
## v[2.0.0](author: emmarher)
  - Add new version, combined half_breath and buffer versions.
  - Add js script to define whether running on the server or not, to select automatically the ws path.
## v[0.4.0]v(author: gelyarellanov)
  - update repository
	
## v[0.3.0](author: emmarher)
  - Add simulador_live.html, ploting the data un time function .
  - Change echosystem.config.json file, run in every core a daphne.

## v[0.3.0](author: emmarher)
  - Change simulador.html in templates, to render in server side to test perfomance, is the v1.2 frontend.
  - Add second websocket (in consumer.py file) backend, send a buffer to frontend.
  - Frontend read and draw canvas using the buffer sent from backend.
  - Use sleep function, using Promise, async and await.
  - Add echosystem.config.json file, to never stop server python, using pm2.


# v[0.2.3](author: emmarher)
  - Add file simuladorv1.html in templates, to render in server side to test perfomance.
  - Change second to simulate in backend, 2 to 1 second, and increase fs to 50.
  
## v[0.2.2](author: emmarher)
  - Add Insert Max value per Second of Value. Elements html
- 
## v[0.2.1](author: emmarher)
  - Add Insert Max value per Second of Value.
## v[0.2.0](author: emmarher)
  - Add canvas js for plotting.
  - add container html  for show max and min values in volume chart.
.
## v[0.1.1] (author: emmarher)
  - Add chartjs for plot in front using django server
## v[0.1.0](author: emmarher)
  - Add commit, init project, add libraries django.
