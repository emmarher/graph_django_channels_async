"""
ASGI config for Root project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
# importar el protocolo de ruteo
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack

from graph.routing import ws_urlpatterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Root.settings')

#application = get_asgi_application()

application = ProtocolTypeRouter({
	'http': get_asgi_application(), #Llave para el protocolo http
	'websocket': AuthMiddlewareStack(URLRouter(ws_urlpatterns)) #Llave para el protocolo ws
	})
