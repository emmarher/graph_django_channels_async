// Declaramos variables globales
let djangoData;
let cont=0;
//let timeSec=0;
const timeSec=[];
const baseSec=12;
const fs=20;
for(let i=0; i<=baseSec*fs;i++){timeSec.push(i/fs)}
console.log(timeSec);
const ctxVolume = document.getElementById('volume').getContext('2d');
const ctxPresion = document.getElementById('presion').getContext('2d')
const ctxFlujo = document.getElementById('flujo').getContext('2d');
const baseLine = new Array(baseSec*fs).fill('.'); //120, 10 por seg.
const baseLinePres = new Array(baseSec*fs).fill('.'); //120, 10 por seg.
const baseLineFluj = new Array(baseSec*fs).fill('.'); //120, 10 por seg.
//console.log(baseLine);
var graphDataVol = {
    type: 'line', //'bar'
    data: {
      display:true,
        labels: timeSec,
        datasets: [{
            label: 'Volumen (mL)',
            data: baseLine,
						fill: false,
            backgroundColor: [
                'rgb(75, 192, 192)',
            ],
						borderColor: 'rgb(25, 66, 144)',
						tension: 0.2,
            borderWidth: 2.5,
            radius: 0.0, //quita las bolitas
        }]
    },
    options: {
			scales: {
            y: {
                beginAtZero: false,
                stacked: true
            },
            x: {
              //type:'linear',
              ticks: {
                // For a category axis, the val is the index so the lookup via getLabelForValue is needed
                callback: function(val, index) {
                  // Hide every 2nd tick label
                  return index % 10 === 0 ? this.getLabelForValue(val) : '';
                },
                color: 'red',
              }
            }
            
        }
			}
}
var graphDataPres = {
  type: 'line', //'bar'
  data: {
    display:true,
      labels: timeSec,
      datasets: [{
          label: 'Presion',
          data: baseLinePres,
          fill: false,
          backgroundColor: [
              'rgb(75, 192, 192)',
          ],
          borderColor: 'rgb(25, 66, 144)',
          tension: 0.0,
          borderWidth: 2,
          radius: 0.0, //quita las bolitas
      }]
  },
  options: {
    responsive: true,
    scales: {
          y: {
              beginAtZero: false,
              stacked: true,
              min: 0,
              max:12,
          },
          x: {
            //type:'linear',
            ticks: {
              // For a category axis, the val is the index so the lookup via getLabelForValue is needed
              callback: function(val, index) {
                // Hide every 2nd tick label
                return index % 10 === 0 ? this.getLabelForValue(val) : '';
              },
              color: 'red',
            },
            grid: {color: 'red'},
          }
          
      }
    }
}
var graphDataFluj = {
  type: 'line', //'bar'
  data: {
    display:true,
      labels: timeSec,
      datasets: [{
          label: 'Flujo',
          data: baseLineFluj,
          fill: false,
          backgroundColor: [
              'rgb(75, 192, 192)',
          ],
          borderColor: 'rgb(25, 66, 144)',
          tension: 0.2,
          borderWidth: 2.5,
          radius: 0.0, //quita las bolitas
      }]
  },
  options: {
    scales: {
          y: {
              beginAtZero: false,
              stacked: true,
              min: -1,
              max:2,
          },
          x: {
            //type:'linear',
            ticks: {
              // For a category axis, the val is the index so the lookup via getLabelForValue is needed
              callback: function(val, index) {
                // Hide every 2nd tick label
                return index % 10 === 0 ? this.getLabelForValue(val) : '';
              },
              color: 'red',
            }
          }
          
      }
    }
}
const volumeChart = new Chart(ctxVolume,graphDataVol );
const presionChart = new Chart(ctxPresion,graphDataPres );
const flujoChart = new Chart(ctxFlujo,graphDataFluj );

//url para enviar peticiones y la url deberia ser la misma
// que definimos en ws_urlpatterns
var socket = new WebSocket('ws://localhost:8000/ws/graph/');

//definir el metodo de mensaje
// DATOS RECIBIDOS
socket.onmessage = function(e){
	djangoData = JSON.parse(e.data);
	console.log(djangoData); // data receive
  cont+=1

	var newVolumeData = graphDataVol.data.datasets[0].data;
	newVolumeData.shift();
	newVolumeData.push(djangoData.volume)
	graphDataVol.data.datasets[0].data = newVolumeData;
	volumeChart.update();
  updatePresion(djangoData);
  updateFlujo(djangoData);

	// value por que la llave enviada desde consumers(backend) es value
	document.querySelector('#app').innerText = djangoData.volume;



  if(cont==fs*3){
    //Llamar la función al backend por mas datos para
    changeVal();
    cont=0;
  }
}

function updatePresion(djangoData) {

  var newPresionData = graphDataPres.data.datasets[0].data;
  newPresionData.shift();
  newPresionData.push(djangoData.presion);
  graphDataPres.data.datasets[0].data = newPresionData;
  presionChart.update();
}
function updateFlujo(djangoData) {

  var newFlujoData = graphDataFluj.data.datasets[0].data;
  newFlujoData.shift();
  newFlujoData.push(djangoData.flujo);
  graphDataFluj.data.datasets[0].data = newFlujoData;
  flujoChart.update();
}


socket.onclose = function(e) {
            console.error('Chat socket closed unexpectedly');
          }
// puede generarse fallos si la conexion no esta abierta, por lo tanto:
/*
socket.onopen = function (event) {
  function sendValue(){
    // Se construye un objeto msg que contiene la informacíon
    var msg = {
      type:"message",
      text: document.getElementById("number").value,
      id: "valor de grafica"
    };
    socket.send();
  }
}
*/
socket.onerror = function(error){
  alert(`[error] ${error.message}`);
}

/*  Rate limiting
every 100ms examine the socket and send more data,
only if all the existing data was sent out

setInterval(() => {
  if(socket.bufferedAmount ==0){
    scoekt.send(moreData());
  }
});
*/
// Onchange event fired
//t.getElementById("number").onchange = function() {changeVal()};


function changeVal() {
  console.log(cont)
  var c = document.getElementById("compliance");
  var r = document.getElementById("resistence");
  console.log('x.value',c.value);
  console.log('r.value',r.value);
  console.log('lastV', djangoData);
  
  socket.send(JSON.stringify({
		'message':c.value,
    'resistence':r.value,
    'lastV':djangoData.volume
	}));

}

function enviar(){
  console.log("button works");
}
