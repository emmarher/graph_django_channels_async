# Grafica en tiempo Real
Grafica utilizando la librearia **chart.js**
Los datos son generados desde el backend enviados al front, los numeros son generados "aleatoriamente" con randint de python, a intervalos de 1 segundo

## Requerimientos base
- Python 3.8
- django4
- djangorestframework
- channels-redis
- redis-cli (instalado en la computadora)
- daphne

## Instalación
```
pip install requirements.txt
```


>El socket solo soporta a una pestaña (usuario)al mismo tiempo, por lo que tenemos que haces uso de redis, que es un servidor para almacenamiento de datos tipo cookies

En el segundo commit se soluciono lo anterior, funcionando ahora el socket asincrono en diferentes pestañas.
```
pip install channels-redis
```

Ejecutamos el servidor de __redis__ con el comando:
```
redis-server
```

Es necesario previamente tener instalado redis server
( http://redis.io )


#### Nota:
El requirements puede incluir librerias adicionales no necesarias para este proyecto, ya que el mismo entorno virtual se utiliza para diferentes proyectos.


**Referencia:** https://www.youtube.com/watch?v=tZY260UyAiE&list=PLe4mIUXfbIqYEOgfh4X_Yz767IntYUSvg&index=4&ab_channel=RedEyedCoderClub

## Ejectuar el servicio utilizando daphne para las peticiones asincronas
```
daphne Root.asgi:application
```
### En servidor
```
daphne -b 0.0.0.0 -p 8000 Root.asgi:application
```

## Se debe verificar que el servidor redis esta conectado
```
redis-cli ping
```

En caso de que no lo este, reiniciar el servidor redis con el siguiente comando
```
sudo service redis-server restart
```

## NOTE:
 En la carpeta llamada front, se encuentra el archivo html y js, ya que al utilizar daphne no interpreta el load static de django.


 ## Ejecutar con pm2 utilizando config file para que se reinicie solo
 pm2 start echosystem.config.json

